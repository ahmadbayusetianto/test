<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

    function __construct(){
		parent::__construct();

		session_start();
		config_kcfinder(array('user_id' => $this->connect_auth->get_user_id()));

		$this->connect_api_url = simpel_url('connect_api_url' );
		$this->connect_url = simpel_url('connect_url' );
		$this->connect_api_url_backend = simpel_url('connect_api_url',true);
		$this->connect_url_backend = simpel_url('connect_url',true);
	}
private $connect_api_url , $connect_url ,$connect_api_url_backend, $connect_url_backend;

/***
Daftar Fungsi Yang Tersedia :
*	__construct()
*	example_document_registration($content = 'home')
*	example_document_registration_post()
*	max_bussinesday_submission_training($content = 'home')
*	max_bussinesday_submission_training_post()
*	limit_submission_training($content = 'home')
*	limit_submission_training_post()
*	todolist($content = 'home', $todolist_id=null)
*	todolist_post($todolist_id=null)
*	type_exam($content = 'home', $type_exam_id=null)
*	type_exam_post($type_exam_id=null)
*	type_training($content = 'home', $type_training_id=null)
*	type_training_post($type_training_id=null)
*	set_default_hour_leasson_instructor($content = 'home', $user_id=null)
*	set_default_hour_leasson_instructor_post($hour_leasson_id = null)
*	config_training($content = 'home', $config_training_id=null)
*	config_training_post($config_training_id=null)
*	config_exam($content = 'home', $config_exam_id=null)
*	config_exam_post($config_exam_id=null)
*	index()
*	oauth($content = 'home', $user_id=null)
*	oauth_data($type=false)
*	oauth_post()
*	email($content = 'home', $user_id=null)
*	logs($content = 'home', $user_id=null)
*	request_datatable($entity="peserta")
*	callApi($url='')
*	categories_faq($content='home', $content_id=null)
*	categories_event($content='home', $content_id=null)
*	categories_guideline($content='home', $content_id=null)
*	categories_news($content='home', $content_id=null)
*	category_post($content_id=null)
*	unit_kompetensi($content = 'home', $unit_kompetensi_id=null)
*	unit_kompetensi_post($unit_kompetensi_id=null)
*	type_surveillance($content = 'home', $type_surveillance_id=null)
*	type_surveillance_post($type_surveillance_id=null)
*	config_surveillance($content = 'home', $config_surveillance_id=null)
*	config_surveillance_post($config_surveillance_id=null)
*	aspek_pemahaman($content = 'home', $aspek_pemahaman_id=null)
*	aspek_pemahaman_post($aspek_pemahaman_id=null)
***/
	function example_document_registration($content = 'home'){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$this->session->set_userdata('redirect',current_url());
		$data['title_page'] = 'Contoh Dokumen Registrasi';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
	
		$data['title_content'] = 'Edit';
		$data['setting_id'] = 'example_document_registration';
		$data['content'] = 'example_document_registration';
	
		$this->load->view('admin/main',$data);
	}
	
	function example_document_registration_post(){
		$input = $this->input->post();
		$input['setting_id'] = 'example_document_registration';
		$setting_id = $input['setting_id'];
		
		if($this->setting_db->save($setting_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
	
    function max_bussinesday_submission_training($content = 'home'){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Set Maksimal Hari Pengajuan Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Edit';
			$data['content'] = 'max_bussinesday_submission_training_add';
		}

		$this->load->view('admin/main',$data);
    }
    
    function max_bussinesday_submission_training_post(){
		$input = $this->input->post();
		$input['setting_id'] = 'max_bussinesday_submission_training';
		$setting_id = $input['setting_id'];
		$input['day'] = (isset($input['day']) ? new MongoInt32($input['day']) : 0 );
		
		
		if($this->setting_db->save($setting_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
    
    	
    function max_register_day($content = 'home'){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Set Maksimal Hari Register Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Edit';
			$data['content'] = 'max_register_day_add';
		}

		$this->load->view('admin/main',$data);
    }
    
    function max_register_day_post(){
		$input = $this->input->post();
		$input['setting_id'] = 'max_register_day';
		$setting_id = $input['setting_id'];
		$input['day'] = (isset($input['day']) ? new MongoInt32($input['day']) : 0 );
		
		
		if($this->setting_db->save($setting_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
	
	function limit_submission_training($content = 'home'){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Limit Pengajuan Pelatihan oleh LPP';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Edit';
			$data['content'] = 'limit_submission_training_add';
		}

		$this->load->view('admin/main',$data);
    }
	
	function regenerate_exam_number($content = 'home')
	{
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Generate Ulang No. Ujian';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Edit';
			$data['content'] = 'regenerate_exam_number';
		}

		$this->load->view('admin/main',$data);
    }
	
	function mail_setting($content = 'home')
	{
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Setting Email';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Edit';
			$data['content'] = 'mail_setting_parameter';
		}

		$this->load->view('admin/main',$data);
    }
	
    function limit_submission_training_post(){
		$input = $this->input->post();
		$input['setting_id'] = 'limit_submission_training';
		$setting_id = $input['setting_id'];
		$input['limit'] = (isset($input['limit']) ? new MongoInt32($input['limit']) : 0 );
		
		
		if($this->setting_db->save($setting_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
	
	function regenerate_exam_number_post()
	{
		$input = $this->input->post();
		$input['username'] = (isset($input['username']) ? $input['username'] : "" );
		$url = $this->config->item('connect_url').'/user/regenerateExamNumber';
		//$data = array('username'=>$input['username']);
		$username = $input['username'];
		$curl = curl_init();
		$data = array('username'=>$username);
		http_build_query($data);
		
		curl_setopt_array($curl, array(CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true, CURLOPT_POSTFIELDS => $data));
		$response = curl_exec($curl);
		//var_dump($response);
		curl_close($curl);
		//$input['exam_number'] = $response;
    }
	
	function mail_setting_post(){
		$input = $this->input->post();
		$input['setting_id'] = 'mail_setting_parameter';
		$setting_id = $input['setting_id'];
		$input['smtp_port'] = (isset($input['smtp_port']) ? new MongoInt32($input['smtp_port']) : 0 );
		$input['smtp_server'] = (isset($input['smtp_server']) ? $input['smtp_server'] : "") ;
		$input['mail_address'] = (isset($input['mail_address']) ? $input['mail_address'] : "" );
		$input['mail_username'] = (isset($input['mail_username']) ? $input['mail_username'] : "" );
		$input['mail_password'] = (isset($input['mail_password']) ? $input['mail_password'] : "" );
		
		if($this->setting_db->save($setting_id, $input)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
		
		redirect($this->session->userdata('redirect'));
    }
    
    function todolist($content = 'home', $todolist_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Todolist';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['todolist_id'] = $todolist_id;
	
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'todolist';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'todolist_add';
	
		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';
	
			$data['content'] = 'todolist_add';
	
		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';
		  
			if($this->todolist_db->delete($todolist_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	
			redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function todolist_post($todolist_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
		
		$input['published'] = (isset($input['published']) ? new MongoInt32($input['published']) : 0);
		
		$data_before_update = array();
		if($query = $this->todolist_db->get($todolist_id)){
			$data_before_update = $query[0];
		}
	
		if($this->todolist_db->save($todolist_id, $input)){
	
			/*save log*/
			$data_log = array(
			'table_name' => 'todolist',
			'table_id' => $todolist_id,
			'log_type' => 'todolist_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
			);
	
			$this->log_db->save(null, $data_log);
	
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	
		redirect($this->session->userdata('redirect'));
    }
    
	function type_exam($content = 'home', $type_exam_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Jenis Ujian';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['type_exam_id'] = $type_exam_id;
	
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'type_exam';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'type_exam_add';
	
		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';
	
			$data['content'] = 'type_exam_add';
	
		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';
		  
			if($this->type_exam_db->delete($type_exam_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	
			redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function type_exam_post($type_exam_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
		
		$input['published'] = (isset($input['published']) ? new MongoInt32($input['published']) : 0);
		
		$data_before_update = array();
		if($query = $this->type_exam_db->get($type_exam_id)){
			$data_before_update = $query[0];
		}
	
		if($this->type_exam_db->save($type_exam_id, $input)){
	
			/*save log*/
			$data_log = array(
			'table_name' => 'type_exam',
			'table_id' => $type_exam_id,
			'log_type' => 'type_exam_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
			);
	
			$this->log_db->save(null, $data_log);
	
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	
		redirect($this->session->userdata('redirect'));
    }
	
    function type_training($content = 'home', $type_training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Jenis Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['type_training_id'] = $type_training_id;
	
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'type_training';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'type_training_add';
	
		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';
	
			$data['content'] = 'type_training_add';
	
		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';
		  
			if($this->type_training_db->delete($type_training_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	
			redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function type_training_post($type_training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
		
		$input['published'] = (isset($input['published']) ? new MongoInt32($input['published']) : 0);
		
		$data_before_update = array();
		if($query = $this->type_training_db->get($type_training_id)){
			$data_before_update = $query[0];
		}
	
		if($this->type_training_db->save($type_training_id, $input)){
	
			/*save log*/
			$data_log = array(
			'table_name' => 'type_training',
			'table_id' => $type_training_id,
			'log_type' => 'type_training_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
			);
	
			$this->log_db->save(null, $data_log);
	
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	
		redirect($this->session->userdata('redirect'));
    }
    
    function set_default_hour_leasson_instructor($content = 'home', $user_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		
		$data['title_page'] = 'Set Default JP Pengajar';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['entity'] = 'narasumber';
		
		if($content == 'home'){
			$this->session->set_userdata('redirect', current_url());
			$data['content'] = 'instructor_hour_leasson';
		}elseif($content == 'status'){
			$activated = $this->input->get('activated');
			$url = $this->connect_api_url_backend.'/user/index/'.$this->connect_auth->get_access_token().'/'.$user_id;
			$input = array('activated' => new MongoInt32($activated));
			$response = call_api_put($url, $input);
			if($response && $response['header_info']['http_code'] == 200){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
			redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }
    
    function set_default_hour_leasson_instructor_post($hour_leasson_id = null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		
		$input = $this->input->post();
		$input['date_assignment'] = array(
			'date' => date('Y-m-d'),
			'year' => date('Y'),
			'month' => date('m'),
			'day' => date('d')
		);
		
		$input['instructor_approved'] = 1;
		$input['instructor_approved_log'] = $this->connect_auth->get_me();
		$input['admin_approved'] = 1;
		$input['admin_approved_log'] = $this->connect_auth->get_me();
		//check training_id dengan instructor_id dan tahun yang sama tdk bisa doble alias unique data
		
		$data_before_update = array();
		
		if($hour_leasson_id && $query = $this->hour_leasson_db->get($hour_leasson_id)){
			$data_before_update = $query[0];
		}
	
		if($this->hour_leasson_db->save($hour_leasson_id, $input)){
	
			/*save log*/
			
			if($hour_leasson_id){
			$data_log = array(
				'table_name' => 'hour_leasson',
				'table_id' => $hour_leasson_id,
				'log_type' => 'hour_leasson_last_update',
				'ip_address' => $this->input->ip_address(),
				'user_id' => $this->connect_auth->get_me()['user_id'],
				'fullname' => $this->connect_auth->get_me()['fullname'],
				'time' => time(),
				'data_before_update' => $data_before_update
			);
	
			$this->log_db->save(null, $data_log);
	
			}
			
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	
		redirect($this->session->userdata('redirect'));
	
    }

    function config_training($content = 'home', $config_training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Nama Pelatihan';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['config_training_id'] = $config_training_id;
	
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'config_training';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'config_training_add';
	
		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';
	
			$data['content'] = 'config_training_add';
	
		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';
		  
			if($this->config_training_db->delete($config_training_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	
			redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }

    function config_training_post($config_training_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
// 		echo_r($input); exit();
		if(!isset($input['published'])){
		    $input['published']= new MongoInt32(0); 
		}else{
		    $input['published'] = new MongoInt32($input['published']);
		}
		$input['deadline']=isset($input['deadline'])?  new MongoInt32($input['deadline']):new MongoInt32(0);

		if(!isset($input['requirement_letter_assignment'])){
		    $input['requirement_letter_assignment']= new MongoInt32(0); 
		}else{
		    $input['requirement_letter_assignment'] = new MongoInt32($input['requirement_letter_assignment']);
		}
		
		if(!isset($input['enable_support_document'])){
		    $input['enable_support_document']= new MongoInt32(0); 
		}else{
		    $input['enable_support_document'] = new MongoInt32($input['enable_support_document']);
		}
		
		if(!isset($input['requirement_certificate'])){
		    $input['requirement_certificate']= new MongoInt32(0); 
		}else{
		    $input['requirement_certificate'] = new MongoInt32($input['requirement_certificate']);
		}
		
		if(!isset($input['requirement_educational'])){
		    $input['requirement_educational'] = array();
		}
		
		if(isset($input['type_training'])){
		    $input['type_training'] = array(
			'id' => $input['type_training'],
			'name' => $this->type_training_db->get($input['type_training'])[0]['name']
		    );
		}
		
		if(!$config_training_id){
		    //save di new aja, edit ga bisa
		    $input['author'] = array(
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'user_id' => $this->connect_auth->get_me()['user_id'],
		    );
		}
		
		
		$data_before_update = array();
		if($query = $this->config_training_db->get($config_training_id)){
		    $data_before_update = $query[0];
		}

		if($this->config_training_db->save($config_training_id, $input)){

		    /*save log*/
		    $data_log = array(
			'table_name' => 'config_training',
			'table_id' => $config_training_id,
			'log_type' => 'config_training_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );

		    $this->log_db->save(null, $data_log);

		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}

		redirect($this->session->userdata('redirect'));
    }
	
	function config_exam($content = 'home', $config_exam_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Nama Ujian';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['config_exam_id'] = $config_exam_id;
	
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'config_exam';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'config_exam_add';
	
		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';
	
			$data['content'] = 'config_exam_add';
	
		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';
		  
			if($this->config_exam_db->delete($config_exam_id)){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	
			redirect($this->session->userdata('redirect'));
		}
	
		$this->load->view('admin/main',$data);
    }

    function config_exam_post($config_exam_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
// 		echo_r($input); exit();
		if(!isset($input['published'])){
		    $input['published'] = new MongoInt32(0);
		}else{
		    $input['published'] = new MongoInt32($input['published']);
		}
		$input['deadline']=isset($input['deadline'])?  new MongoInt32($input['deadline']):new MongoInt32(0);
		if(!isset($input['requirement_letter_assignment'])){
		    $input['requirement_letter_assignment']= new MongoInt32(0); 
		}else{
		    $input['requirement_letter_assignment'] = new MongoInt32($input['requirement_letter_assignment']);
		}
		
		if(!isset($input['enable_support_document'])){
		    $input['enable_support_document']= new MongoInt32(0); 
		}else{
		    $input['enable_support_document'] = new MongoInt32($input['enable_support_document']);
		}
		
		if(!isset($input['requirement_certificate'])){
		    $input['requirement_certificate']= new MongoInt32(0); 
		}else{
		    $input['requirement_certificate'] = new MongoInt32($input['requirement_certificate']);
		}
		
		if(!isset($input['requirement_educational'])){
		    $input['requirement_educational'] = array();
		}
		
		if(isset($input['type_exam'])){
		    $input['type_exam'] = array(
			'id' => $input['type_exam'],
			'name' => $this->type_exam_db->get($input['type_exam'])[0]['name']
		    );
		}

		$input['okupasi'] = $input['type_okupasi'];

		if(isset($input['type_okupasi'])){
			$input['type_okupasi'] = array(
				'id' => $input['type_okupasi'],
				'name' => $this->init_config_db->get_type_okupasi($input['type_okupasi'])
			);
		}

		$input['requirement_portfolio_exam']=isset($input['requirement_portfolio_exam'])?$input['requirement_portfolio_exam']:0;
		$input['requirement_portfolio']= new MongoInt32(0);
		
		if(!$config_exam_id){
		    //save di new aja, edit ga bisa
		    $input['author'] = array(
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'user_id' => $this->connect_auth->get_me()['user_id'],
		    );
		}
		
		
		$data_before_update = array();
		if($query = $this->config_exam_db->get($config_exam_id)){
		    $data_before_update = $query[0];
		}

//		die('<pre>'.$config_exam_id.echo_r($input,1));
		if($this->config_exam_db->save($config_exam_id, $input)){

		    /*save log*/
		    $data_log = array(
			'table_name' => 'config_exam',
			'table_id' => $config_exam_id,
			'log_type' => 'config_exam_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );

		    $this->log_db->save(null, $data_log);

		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}

		redirect($this->session->userdata('redirect'));
    }

    function index(){
		redirect('superadmin');
    }
	
	function oauth($content = 'home', $user_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Setting Oauth';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());
		    $data['content'] = 'setting_oauth';
			$data['content_table'] = true;
			$data['content_entity'] = $entity='oauth';
			$this->apiParams['role']='superadmin';
			$response=$this->request_datatable($entity);
			$data['result'] = isset($response['result'])?$response['result']:$response;
					    
		}
		elseif($content == 'add'){
		    $data['title_content'] = 'Tambah';
		    $data['content'] = 'setting_oauth_add';
		}

		if(!isset($data['content'])){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Selamat Menggunakan!'));
			$url=$this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3);
			redirect(site_url($url),1);
		}
	
		$this->load->view('admin/main',$data);
    }
	function oauth_data($type=false){
		$entity="lpp";//substr($this->uri->segment(3),0,-5);
		$response=$this->request_datatable($entity);
		$result = isset($response['result'])?$response['result']:array();
		if($type=='debug'){
		echo '<pre>';echo_r($response,1);
		}
		else{ 
			print json_encode($result);
		}
		exit();
	}
	function oauth_post(){
		$this->apiParams['post']=$this->input->post();
		
		$token = $this->connect_auth->get_access_token();
		$url=$this->connect_api_url_backend.'/datatable/oauth/'.$token;
		$this->apiParams['token']=$token;
		$this->apiParams['role']='superadmin';
		$this->apiParams['action']='post';
		$this->apiParams['entity']='oauth';
		$response=$this->callApi($url);
	//	echo $url.'???<pre>'.echo_r($response,1).'</pre>';

		redirect('superadmin/setting/oauth',1);
	}

	function email($content = 'home', $id=null){
		if(!run_in_development()) redirect('superadmin/dashboard');
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Log Email';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$token = $this->connect_auth->get_access_token();
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());

		$apiParams['token']=$token;
		$apiParams['role']='superadmin';
			$apiParams['entity']='email';
			$data['content'] = 'setting_email';
			$data['content_table'] = true;
			$data['content_entity'] = $entity='email';
			$apiParams['get']=array('length'=>33);
			$apiParams['url']=site_url('superadmin/setting/email/read/');
			
			$this->apiParams=$apiParams;
			$this->apiParams['role']='superadmin';
			
			$url=$this->connect_api_url_backend.'/datatable/email/'.$token;
			$response=$this->callApi($url);
			$data['result'] = isset($response['result'])?$response['result']:$response;
		//	echo '<pre>'.print_r($data['result'] ,1); die();
		}
		elseif($content == 'read'){
			//$data['content'] = 'setting_email_view';
			$data['id']=$id;
			$p=$this->load->view('admin/content/setting_email_view',$data,true);
			exit($p);
		}
		
		if(!isset($data['content'])){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Selamat Menggunakan!'));
			$url=$this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3);
			redirect(site_url($url),1);
		}
	
		$this->load->view('admin/main',$data);
    }
	
	function logs($content = 'home', $user_id=null){
		if(!run_in_development()) redirect('superadmin/dashboard');
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Log System';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$token = $this->connect_auth->get_access_token();
		if($content == 'home'){
		    $this->session->set_userdata('redirect', current_url());
			$data['content_entity']='logs';
			$data['content'] = 'setting_logs';
		}
		elseif($content == 'add'){
			
		}
	//	die('stop');
		if(!isset($data['content'])){
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Selamat Menggunakan!'));
			$url=$this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3);
			redirect(site_url($url),1);
		}
	
		$this->load->view('admin/main',$data);
    }
	
	private function request_datatable($entity="peserta"){		 
		$token = $this->connect_auth->get_access_token();
		$this->apiParams['token']=$token; //tidak dibaca sebenarnya
		$url=$this->connect_api_url_backend.'/datatable/oauth/'.$token;///'.$token.'/'.$entity;		
		$this->apiParams['entity']=$entity;
		$this->apiParams['get']=$this->input->get();
		$this->apiParams['get']['site_url']=site_url("superadmin/management_user/".substr($this->uri->segment(3),0,-5));
		$this->apiParams['get']['connect_url']= $this->connect_url;
		$response=$this->callApi($url);
//		$result = isset($response['result'])?$response['result']:false;
//		if($result===false)die( $response );
	 return $response;
	}

	private function callApi($url=''){
		$params=array('data'=>json_encode($this->apiParams));
		$response=call_api_post($url,$params);
		log_message('info','call API url:'.$url.'| params data:'.json_encode($this->apiParams));
		$body=isset($response['body'])?@json_decode($response['body'],true):false;
		if(!is_array($body)){
			log_message('info','respon not array?');
			$body= $response['body'];			
		}
		if(!isset($body['result']))return $body;
		$body['result']= json_decode( base64_decode($body['result']) , true );
		return $body;
	}
	
	function categories_faq($content='home', $content_id=null){
		$data['title'] = 'Categories';
		$data['title_page'] = 'FAQ';
		$data['content_id'] = $content_id;
		$data['category_type'] = 'faq';
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'categories';
		} else if($content == 'add') {
			$data['title_content'] = 'Tambah';
			$data['content'] = 'categories_add';
		} else if($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['content'] = 'categories_add';
		} else if($content == 'delete'){
			if($this->categories_db->delete($content_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
	}
	
	function categories_event($content='home', $content_id=null){
		$data['title'] = 'Categories';
		$data['title_page'] = 'Agenda';
		$data['content_id'] = $content_id;
		$data['category_type'] = 'event';
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'categories';
		} else if($content == 'add') {
			$data['title_content'] = 'Tambah';
			$data['content'] = 'categories_add';
		} else if($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['content'] = 'categories_add';
		} else if($content == 'delete'){
			if($this->categories_db->delete($content_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
	}

	function categories_guideline($content='home', $content_id=null){
		$data['title'] = 'Categories';
		$data['title_page'] = 'Pedoman dan Panduan';
		$data['content_id'] = $content_id;
		$data['category_type'] = 'guideline';
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'categories';
		} else if($content == 'add') {
			$data['title_content'] = 'Tambah';
			$data['content'] = 'categories_add';
		} else if($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['content'] = 'categories_add';
		} else if($content == 'delete'){
			if($this->categories_db->delete($content_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
	}
	
	function categories_news($content='home', $content_id=null){
		$data['title'] = 'Categories';
		$data['title_page'] = 'Berita';
		$data['content_id'] = $content_id;
		$data['category_type'] = 'news';
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'categories';
		} else if($content == 'add') {
			$data['title_content'] = 'Tambah';
			$data['content'] = 'categories_add';
		} else if($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['content'] = 'categories_add';
		} else if($content == 'delete'){
			if($this->categories_db->delete($content_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
	}
	
	function categories_users($content='home', $content_id=null){
        redirect("superadmin/setting/role/".$content);
	}

    function role($content='home', $content_id=null){
    //    $this->load->model('user_role_db');
		$data['title'] = 'Categories';
		$data['title_page'] = 'User Role (aksi)';
		$data['content_id'] = $content_id;
		$data['category_type'] = 'role';
$data['target_db'] = 'categories_db';
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'categori_role';
		} else if($content == 'add') {
			$data['title_content'] = 'Tambah';
			$data['content'] = 'categori_role_add';
		} else if($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['content'] = 'categori_role_add';
		}
		else if($content == 'upload'){
			$this->categories_db->uploadConnect( $data['category_type'] );
			jsGoto($this->session->userdata('redirect'),1);
		}
		else if($content == 'delete'){
			if($this->categories_db->delete($content_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
	}
	function entity($content='home', $content_id=null){
        $this->load->model('user_entity_db');
		$data['title'] = 'Categories';
		$data['title_page'] = 'User Entity';
		$data['content_id'] = $content_id;
		$data['category_type'] = 'entity';
		$data['target_db'] = 'user_entity_db';

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'categori_role';
		} else if($content == 'add') {
			$data['title_content'] = 'Tambah';
			$data['content'] = 'categori_role_add';
		} else if($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['content'] = 'categori_role_add';
		}
		else if($content == 'upload'){
			$this->categories_db->uploadConnect( $data['category_type'] );
			jsGoto($this->session->userdata('redirect'),1);
		}
		else if($content == 'delete'){
			if($this->categories_db->delete($content_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
	}
	function academic_educational($content='home', $content_id=null){
        $this->load->model('academic_db');
		$data['title'] = 'Categories';
		$data['title_page'] = 'User Entity';
		$data['content_id'] = $content_id;
		$data['category_type'] = 'academic_educational';
		$data['target_db'] = 'academic_db';

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'categori_academic_educational';
		} else if($content == 'add') {
			$data['title_content'] = 'Tambah';
			$data['content'] = 'category_academic_educational_add';
		} else if($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['content'] = 'category_academic_educational_add';
		}
		else if($content == 'upload'){
			$this->categories_db->uploadConnect( $data['category_type'] );
			jsGoto($this->session->userdata('redirect'),1);
		}
		else if($content == 'delete'){
			if($this->categories_db->delete($content_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
	}
	function academic_degre($content='home', $content_id=null){
		redirect('superadmin/setting/academic_degree/'.$content.'/'.$content_id);
	}
	function academic_degree($content='home', $content_id=null){
        $this->load->model('academic_db');
		$data['title'] = 'Categories';
		$data['title_page'] = 'User Entity';
		$data['content_id'] = $content_id;
		$data['category_type'] = 'academic_degree';
		$data['target_db'] = 'academic_db';

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'categori_academic_degre';
		} else if($content == 'add') {
			$data['title_content'] = 'Tambah';
			$data['content'] = 'category_academic_degre_add';
		} else if($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['content'] = 'category_academic_degre_add';
		}
		else if($content == 'upload'){
			$this->categories_db->uploadConnect( $data['category_type'] );
			jsGoto($this->session->userdata('redirect'),1);
		}
		else if($content == 'delete'){
			if($this->categories_db->delete($content_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}
	    
		    redirect($this->session->userdata('redirect'));
		}
		
		$this->load->view('admin/main',$data);
	}

    function category_post($content_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
		$input['category_id'] = $content_id;
	    $target_db="categories_db";
		if( $input['category_type']=='academic_educational'|| $input['category_type']=='academic_degree'){
			$target_db="academic_db";
			$this->load->model($target_db);
			$query=$this->$target_db->get();
//			echo'<pre><hr/>';print_r($query );
			$data=array(
			'educational_level'=>$query['educational_level'],
			'academic_degree'=>$query['academic_degree']
			);
			if( $input['category_type']=='academic_educational'){
				if($content_id==null){
					$data['educational_level'][]=$input['nama'];
				}
				else{
					$data['educational_level'][$content_id]=$input['nama'];
				}
			}
			if( $input['category_type']=='academic_degree'){
				$academic_degree=array(
					'degree'=>strtolower($input['degree']),
					'sort_name'=>$input['sort_name'],
					'long_name'=>$input['long_name']
				);
				if($content_id==null){
					$data['academic_degree'][]=$academic_degree;
				}
				else{
					$data['academic_degree'][$content_id]=$academic_degree;
				}
			}
//			echo '<pre>'.print_r($data,1);exit();
			$content_id=$query['_id']->{'$id'};
			foreach($data as $key=>$val)
				$input[$key]=$val;

			$input['page_id']=$content_id;
			unset($input['nama'],$input['category_id']);
        }
		if( $input['category_type']=='role'){
		//	$target_db="category_db";
		//	$this->load->model($target_db);
        }
		if( $input['category_type']=='entity'){
			$target_db="user_entity_db";
			$this->load->model($target_db);
        }
//echo '<pre>'.print_r($input,1);die($content_id);

		$input['author'] = array(
		    'fullname' => $this->connect_auth->get_me()['fullname'],
		    'user_id' => $this->connect_auth->get_me()['user_id'],
		);
	
		$data_before_update = array();
		if($query = $this-> $target_db ->get($content_id)){
		    $data_before_update = isset($query[0])?$query[0]:false;
		}
//                print_r($input);die();
//unset($input['category_type']);
		if($this-> $target_db ->save($content_id, $input)){
		    /*save log*/
		    $data_log = array(
			'table_name' => 'pages',
			'table_id' => $content_id,
			'log_type' => $input['page_id'].'_last_update',
			'ip_address' => $this->input->ip_address(),
			'user_id' => $this->connect_auth->get_me()['user_id'],
			'fullname' => $this->connect_auth->get_me()['fullname'],
			'time' => time(),
			'data_before_update' => $data_before_update
		    );
	    
		    $this->log_db->save(null, $data_log);
	    
		    $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}
        else{
		    $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}
	    
		redirect($this->session->userdata('redirect'));
    }

	function skkni($content = 'home', $skkni_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'SKNNI';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['skkni_id'] = $skkni_id;

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'skkni';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'skkni_add';

		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';

			$data['content'] = 'skkni_add';

		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';

			if($this->type_training_db->delete($skkni_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}

			redirect($this->session->userdata('redirect'));
		}

		$this->load->view('admin/main',$data);
	}

	function skkni_post($skkni_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();

		$input['published'] = (isset($input['published']) ? new MongoInt32($input['published']) : 0);

		$data_before_update = array();
		if($query = $this->skkni_db->get($skkni_id)){
			$data_before_update = $query[0];
		}

		if($this->skkni_db->save($skkni_id, $input)){

			/*save log*/
			$data_log = array(
				'table_name' => 'skkni',
				'table_id' => $skkni_id,
				'log_type' => 'type_surveillance_last_update',
				'ip_address' => $this->input->ip_address(),
				'user_id' => $this->connect_auth->get_me()['user_id'],
				'fullname' => $this->connect_auth->get_me()['fullname'],
				'time' => time(),
				'data_before_update' => $data_before_update
			);

			$this->log_db->save(null, $data_log);

			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}

		redirect($this->session->userdata('redirect'));
	}

	function unit_kompetensi($content = 'home', $skkni_id=null, $okupasi_unit_kompetensi_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Unit Kompetensi';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['okupasi_unit_kompetensi_id'] = $okupasi_unit_kompetensi_id;

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$query = $this->okupasi_unit_kompetensi_db->get_all(array('skkni_id' => $skkni_id));
			$content = array();
			if($query){
				$content = $query;
			}

			$data['okupasi_unit_kompetensi'] =  $content;
			$data['skkni_id'] = $skkni_id;
			$data['content'] = 'unit_kompetensi';
		}elseif($content == 'add'){

			$data['skkni_id'] = $skkni_id;
			$data['title_content'] = 'Tambah';
			$data['content'] = 'unit_kompetensi_add';

		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';
			$data['skkni_id'] = $skkni_id;
			$data['content'] = 'unit_kompetensi_add';

		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';

			if($this->unit_kompetensi_db->delete($okupasi_unit_kompetensi_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}

			redirect($this->session->userdata('redirect'));
		}

		$this->load->view('admin/main',$data);
	}

	function unit_kompetensi_post($okupasi_unit_kompetensi_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$return = true;
		$data = array();
		$arr_unit_id = array();
		$input = $this->input->post();

		if(isset($input['type_okupasi'])){
			$mode_type_okupasi = $this->init_config_db->get_type_okupasi($input['type_okupasi']);
			$data['type_okupasi'] = array(
				'id' => isset($input['type_okupasi']) ? $input['type_okupasi'] : "",
				'name' => isset($mode_type_okupasi) ? $mode_type_okupasi : ""
			);
		}

		$data['skkni_id'] =  isset($input['skkni_id']) ? $input['skkni_id'] : '';
		$data['skkni_refrence'] =  isset($input['skkni_refrence']) ? $input['skkni_refrence'] : '';

		if(!isset($input['published'])){
			$data['published']= new MongoInt32(0);
		}else{
			$data['published'] = new MongoInt32($input['published']);
		}

		if($result = $this->okupasi_unit_kompetensi_db->save($okupasi_unit_kompetensi_id, $data))
		{
			$okupasi_unit_id = is_null($okupasi_unit_kompetensi_id) ? $result->{'$id'} : $okupasi_unit_kompetensi_id;
			foreach($input['unit_kompetensi'] as $unit)
			{
				$unit_id = isset($unit['hidden_unit_id']) ? $unit['hidden_unit_id'] : null;
				$data_unit['okupasi_unit_kompetensi_id'] = $okupasi_unit_id;
				$data_unit['code'] = isset($unit['code']) ? $unit['code'] : null;
				$data_unit['judul'] = isset($unit['judul']) ? $unit['judul'] : null;
				$data_unit['refrence_unit'] = isset($unit['ref_unit']) ? $unit['ref_unit'] : null;

				$data_before_update = array();
				if($query = $this->unit_kompetensi_db->get($unit_id)){
					$data_before_update = $query[0];
				}

				if($result_unit = $this->unit_kompetensi_db->save($unit_id, $data_unit)){

					$unit_kompetensi_id = is_null($unit_id) ? $result_unit->{'$id'} : $unit_id;

					array_push($arr_unit_id, $unit_kompetensi_id);
					/*save log*/
					$data_log = array(
						'table_name' => 'unit_kompetensi',
						'table_id' => $unit_kompetensi_id,
						'log_type' => 'unit_kompetensi_last_update',
						'ip_address' => $this->input->ip_address(),
						'user_id' => $this->connect_auth->get_me()['user_id'],
						'fullname' => $this->connect_auth->get_me()['fullname'],
						'time' => time(),
						'data_before_update' => $data_before_update
					);

					$this->log_db->save(null, $data_log);

					if(!$this->unit_kompetensi_portofolio($unit_kompetensi_id, $unit))
					{
						$return = false;
						break;
					}

					if(!$this->unit_kompetensi_self_assessment($unit_kompetensi_id, $unit))
					{
						$return = false;
						break;
					}

				}else{
					$return = false;
					break;
				}
			}

			if(!empty($arr_unit_id))
			{
				$this->delete_unit_not_exist($okupasi_unit_kompetensi_id, $arr_unit_id);
			}
		}
		else
		{
			$return = false;
		}


		if($return)
		{
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}

		redirect($this->session->userdata('redirect'));
	}

	function delete_unit_not_exist($okupasi_unit_kompetensi_id =null, $arr_unit_id = array())
	{
		if(!is_null($okupasi_unit_kompetensi_id))
		{
			$query_unit = $this->unit_kompetensi_db->get_all(array('okupasi_unit_kompetensi_id' => $okupasi_unit_kompetensi_id));
			if($query_unit)
			{
				foreach($query_unit as $unit)
				{
					$id = $unit['_id']->{'$id'};
					if(!in_array($id, $arr_unit_id))
					{
						$this->unit_kompetensi_db->delete($id);
						$query_porto = $this->unit_kompetensi_portofolio_db->get_all(array('unit_kompetensi_id' => $id));
						$query_assessment = $this->unit_kompetensi_self_assessment_db->get_all(array('unit_kompetensi_id' => $id));
						if(!empty($query_porto))
						{
							foreach($query_porto as $porto)
							{
								$this->unit_kompetensi_portofolio_db->delete($porto['_id']->{'$id'});
							}
						}
						if(!empty($query_assessment))
						{
							foreach($query_assessment as $assessment)
							{
								$this->unit_kompetensi_self_assessment_db->delete($assessment['_id']->{'$id'});
							}
						}


					}
				}
			}
		}

	}

	function  unit_kompetensi_portofolio($unit_id, $unit)
	{
		$return = false;
		$arr_komp_porto_id = array();
		foreach($unit['portofolio'] as $portofolio)
		{

			$unit_komp_porto_id = isset($portofolio['hidden_porto_id']) ? $portofolio['hidden_porto_id'] : null;
			$portofolio_data['unit_kompetensi_id'] = $unit_id;
			$portofolio_data['judul'] = $portofolio['judul'];
			$data_before_update = array();

			if($query = $this->unit_kompetensi_db->get($unit_komp_porto_id)){
				$data_before_update = $query[0];
			}


			if($result_portofolio = $this->unit_kompetensi_portofolio_db->save($unit_komp_porto_id, $portofolio_data)){
				$porto_id = is_null($unit_komp_porto_id) ? $result_portofolio->{'$id'} : $unit_komp_porto_id;
				array_push($arr_komp_porto_id, $porto_id);


				$data_log = array(
					'table_name' => 'unit_kompetensi_portfolio',
					'table_id' => $porto_id,
					'log_type' => 'unit_kompetensi_portofoliolast_update',
					'ip_address' => $this->input->ip_address(),
					'user_id' => $this->connect_auth->get_me()['user_id'],
					'fullname' => $this->connect_auth->get_me()['fullname'],
					'time' => time(),
					'data_before_update' => $data_before_update
				);

				$this->log_db->save(null, $data_log);

				$return = true;
			}else{

				$return = false;
				break;
			}
		}
		if(!empty($arr_komp_porto_id))
		{
			$this->delete_portofolio_unit_not_exist($unit_id, $arr_komp_porto_id);
		}
		return $return;
	}

	function delete_portofolio_unit_not_exist($unit_id=null, $arr_komp_porto_id = array())
	{
		if(!is_null($unit_id))
		{
			$result = $result = $this->unit_kompetensi_portofolio_db->get_all(array('unit_kompetensi_id' => $unit_id));
			if($result)
			{
				foreach($result as $porto)
				{
					$id = $porto['_id']->{'$id'};
					if(!in_array($id, $arr_komp_porto_id))
					{
						$this->unit_kompetensi_portofolio_db->delete($id);
					}
				}
			}
		}

	}

	function unit_kompetensi_self_assessment($unit_id, $unit)
	{
		$return = false;
		$arr_komp_assessment_id = array();
		foreach($unit['self_assessment'] as $self_assessment)
		{
			$unit_komp_assessment_id = isset($self_assessment['hidden_self_assessment_id']) ? $self_assessment['hidden_self_assessment_id'] : null;
			$assessment_data['unit_kompetensi_id'] = $unit_id;
			$assessment_data['judul'] = $self_assessment['judul'];
			$data_before_update = array();

			if($query = $this->unit_kompetensi_db->get($unit_komp_assessment_id)){
				$data_before_update = $query[0];
			}

			if($result = $this->unit_kompetensi_self_assessment_db->save($unit_komp_assessment_id, $assessment_data)){
				$assessment_id = is_null($unit_komp_assessment_id) ? $result->{'$id'} : $unit_komp_assessment_id;
				array_push($arr_komp_assessment_id, $assessment_id);


				$data_log = array(
					'table_name' => 'unit_kompetensi_self_assessment',
					'table_id' => $assessment_id,
					'log_type' => 'unit_kompetensi_portofoliolast_update',
					'ip_address' => $this->input->ip_address(),
					'user_id' => $this->connect_auth->get_me()['user_id'],
					'fullname' => $this->connect_auth->get_me()['fullname'],
					'time' => time(),
					'data_before_update' => $data_before_update
				);

				$this->log_db->save(null, $data_log);

				$return = true;
			}else{

				$return = false;
				break;
			}
		}
		if(!empty($arr_komp_assessment_id))
		{
			$this->delete_assessment_unit_not_exist($unit_id, $arr_komp_assessment_id);
		}
		return $return;
	}

	function delete_assessment_unit_not_exist($unit_id=null, $arr_komp_assessment_id = array())
	{
		if(!is_null($unit_id))
		{
			$result = $result = $this->unit_kompetensi_self_assessment_db->get_all(array('unit_kompetensi_id' => $unit_id));
			if($result)
			{
				foreach($result as $assessment)
				{
					$id = $assessment['_id']->{'$id'};
					if(!in_array($id, $arr_komp_assessment_id))
					{
						$this->unit_kompetensi_self_assessment_db->delete($id);
					}
				}
			}
		}

	}

	function type_surveillance($content = 'home', $type_surveillance_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Jenis Surveillance';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['type_surveillance_id'] = $type_surveillance_id;

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'type_surveillance';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'type_surveillance_add';

		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';

			$data['content'] = 'type_surveillance_add';

		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';

			if($this->type_training_db->delete($type_surveillance_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}

			redirect($this->session->userdata('redirect'));
		}

		$this->load->view('admin/main',$data);
	}

	function type_surveillance_post($type_surveillance_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();

		$input['published'] = (isset($input['published']) ? new MongoInt32($input['published']) : 0);

		$data_before_update = array();
		if($query = $this->type_surveillance_db->get($type_surveillance_id)){
			$data_before_update = $query[0];
		}

		if($this->type_surveillance_db->save($type_surveillance_id, $input)){

			/*save log*/
			$data_log = array(
				'table_name' => 'type_surveillance',
				'table_id' => $type_surveillance_id,
				'log_type' => 'type_surveillance_last_update',
				'ip_address' => $this->input->ip_address(),
				'user_id' => $this->connect_auth->get_me()['user_id'],
				'fullname' => $this->connect_auth->get_me()['fullname'],
				'time' => time(),
				'data_before_update' => $data_before_update
			);

			$this->log_db->save(null, $data_log);

			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}

		redirect($this->session->userdata('redirect'));
	}

	function config_surveillance($content = 'home', $config_surveillance_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Nama Surveillance';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['config_surveillance_id'] = $config_surveillance_id;

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'config_surveillance';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'config_surveillance_add';

		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';

			$data['content'] = 'config_surveillance_add';

		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';

			if($this->config_surveillance_db->delete($config_surveillance_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}

			redirect($this->session->userdata('redirect'));
		}

		$this->load->view('admin/main',$data);
	}

	function config_surveillance_post($config_surveillance_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$input = $this->input->post();
// 		echo_r($input); exit();
		if(!isset($input['published'])){
			$input['published'] = new MongoInt32(0);
		}else{
			$input['published'] = new MongoInt32($input['published']);
		}
		$input['deadline']=isset($input['deadline'])?  new MongoInt32($input['deadline']):new MongoInt32(0);
		if(!isset($input['requirement_letter_assignment'])){
			$input['requirement_letter_assignment']= new MongoInt32(0);
		}else{
			$input['requirement_letter_assignment'] = new MongoInt32($input['requirement_letter_assignment']);
		}

		if(!isset($input['enable_support_document'])){
			$input['enable_support_document']= new MongoInt32(0);
		}else{
			$input['enable_support_document'] = new MongoInt32($input['enable_support_document']);
		}

		if(!isset($input['requirement_certificate'])){
			$input['requirement_certificate']= new MongoInt32(0);
		}else{
			$input['requirement_certificate'] = new MongoInt32($input['requirement_certificate']);
		}

		if(!isset($input['requirement_educational'])){
			$input['requirement_educational'] = array();
		}

		if(isset($input['type_surveillance'])){
			$input['type_surveillance'] = array(
				'id' => $input['type_surveillance'],
				'name' => $this->type_surveillance_db->get($input['type_surveillance'])[0]['code'],
				'name' => $this->type_surveillance_db->get($input['type_surveillance'])[0]['name']
			);
		}

		if(isset($input['type_okupasi']) && !empty($input['type_okupasi'])){
			$input['type_okupasi'] = array(
				'id' => $input['type_okupasi'],
				'name' => $this->init_config_db->get_type_okupasi($input['type_okupasi'])
			);
		}

		$input['requirement_portfolio_surveillance']=isset($input['requirement_portfolio_surveillance'])?$input['requirement_portfolio_surveillance']:0;
		$input['requirement_portfolio']= new MongoInt32(0);

		if(!$config_surveillance_id){
			//save di new aja, edit ga bisa
			$input['author'] = array(
				'fullname' => $this->connect_auth->get_me()['fullname'],
				'user_id' => $this->connect_auth->get_me()['user_id'],
			);
		}


		$data_before_update = array();
		if($query = $this->config_surveillance_db->get($config_surveillance_id)){
			$data_before_update = $query[0];
		}

//		die('<pre>'.$config_exam_id.echo_r($input,1));
		if($this->config_surveillance_db->save($config_surveillance_id, $input)){

			/*save log*/
			$data_log = array(
				'table_name' => 'config_surveillance',
				'table_id' => $config_surveillance_id,
				'log_type' => 'config_surveillance_last_update',
				'ip_address' => $this->input->ip_address(),
				'user_id' => $this->connect_auth->get_me()['user_id'],
				'fullname' => $this->connect_auth->get_me()['fullname'],
				'time' => time(),
				'data_before_update' => $data_before_update
			);

			$this->log_db->save(null, $data_log);

			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}

		redirect($this->session->userdata('redirect'));
	}

	function aspek_pemahaman($content = 'home', $aspek_pemahaman_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Aspek Pemahaman';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['aspek_pemahaman_id'] = $aspek_pemahaman_id;

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['content'] = 'aspek_pemahaman';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'aspek_pemahaman_add';

		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';

			$data['content'] = 'aspek_pemahaman_add';

		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';

			if($this->aspek_pemahaman_db->delete($aspek_pemahaman_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}

			redirect($this->session->userdata('redirect'));
		}

		$this->load->view('admin/main',$data);
	}

	function aspek_pemahaman_post($aspek_pemahaman_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));

		$input = $this->input->post();

		if(isset($input['type_surveillance'])){
			$input['type_surveillance'] = array(
				'id' => $input['type_surveillance'],
				'name' => $this->type_surveillance_db->get($input['type_surveillance'])[0]['name']
			);
		}

		if(!isset($input['published'])){
			$input['published']= new MongoInt32(0);
		}else{
			$input['published'] = new MongoInt32($input['published']);
		}


		$data_before_update = array();
		if($query = $this->aspek_pemahaman_db->get($aspek_pemahaman_id)){
			$data_before_update = $query[0];
		}

//		die('<pre>'.$config_exam_id.print_r($input,1));
		if($this->aspek_pemahaman_db->save($aspek_pemahaman_id, $input)){

			/*save log*/
			$data_log = array(
				'table_name' => 'aspek_pemahamn',
				'table_id' => $aspek_pemahaman_id,
				'log_type' => 'aspek_pemahaman_last_update',
				'ip_address' => $this->input->ip_address(),
				'user_id' => $this->connect_auth->get_me()['user_id'],
				'fullname' => $this->connect_auth->get_me()['fullname'],
				'time' => time(),
				'data_before_update' => $data_before_update
			);

			$this->log_db->save(null, $data_log);

			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}

		redirect($this->session->userdata('redirect'));
	}

	function unit_kompetensi_surveillance($content = 'home', $okupasi_unit_surveillance_id=null){
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Unit Kompetensi';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		$data['okupasi_unit_surveillance_id'] = $okupasi_unit_surveillance_id;

		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$query = $this->okupasi_unit_surveillance_db->get_all();
			$content = array();
			if($query){
				$content = $query;
			}

			$data['okupasi_unit_surveillance'] =  $content;
			$data['content'] = 'unit_kompetensi_surveillance';
		}elseif($content == 'add'){
			$data['title_content'] = 'Tambah';
			$data['content'] = 'unit_kompetensi_surveillance_add';

		}elseif($content == 'edit'){
			$data['title_content'] = 'Edit';

			$data['content'] = 'unit_kompetensi_surveillance_add';

		}elseif($content == 'delete'){
			//$data['content'] = 'user_admin';

			if($this->okupasi_unit_surveillance_db->delete($okupasi_unit_surveillance_id)){
				$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
			}else{
				$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
			}

			redirect($this->session->userdata('redirect'));
		}

		$this->load->view('admin/main',$data);
	}

	function unit_kompetensi_surveillance_post($okupasi_unit_surveillance_id=null)
	{
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data = array();
		$return = true;
		$arr_unit_id = array();
		$input = $this->input->post();


		if(isset($input['type_okupasi']))
		{
			$mode_type_okupasi = $this->init_config_db->get_type_okupasi($input['type_okupasi']);
			$data['type_okupasi'] = array(
				'id' => isset($input['type_okupasi']) ? $input['type_okupasi'] : "",
				'name' => isset($mode_type_okupasi) ? $mode_type_okupasi : ""
			);
		}

		if(!isset($input['published']))
		{
			$data['published']= new MongoInt32(0);
		}else{
			$data['published'] = new MongoInt32($input['published']);
		}

		if($result = $this->okupasi_unit_surveillance_db->save($okupasi_unit_surveillance_id, $data))
		{
			$okupasi_unit_id = is_null($okupasi_unit_surveillance_id) ? $result->{'$id'} : $okupasi_unit_surveillance_id;
			foreach($input['unit_kompetensi_surveillance'] as $unit)
			{
				$unit_id = isset($unit['hidden_unit_id']) ? $unit['hidden_unit_id'] : null;
				$data_unit['okupasi_unit_surveillance_id'] = $okupasi_unit_id;
				$data_unit['code'] = $unit['code'];
				$data_unit['judul'] = $unit['judul'];

				$data_before_update = array();

				if($query = $this->unit_kompetensi_surveillance_db->get($unit_id)){
					$data_before_update = $query[0];
				}

				if($result_unit =$this->unit_kompetensi_surveillance_db->save($unit_id, $data_unit)){

					$unit_surveillance_id = is_null($unit_id) ? $result_unit->{'$id'} : $unit_id;

					array_push($arr_unit_id, $unit_surveillance_id);

					/*save log*/
					$data_log = array(
						'table_name' => 'unit_kompetensi_surveillance',
						'table_id' => $unit_surveillance_id,
						'log_type' => 'cunit_kompetensi_last_update',
						'ip_address' => $this->input->ip_address(),
						'user_id' => $this->connect_auth->get_me()['user_id'],
						'fullname' => $this->connect_auth->get_me()['fullname'],
						'time' => time(),
						'data_before_update' => $data_before_update
					);

					$this->log_db->save(null, $data_log);

					$return = true;
				}else{
					$return = false;
					break;
				}
			}

			if(!empty($arr_unit_id))
			{
				$this->delete_unit_surveillance_not_exist($okupasi_unit_surveillance_id, $arr_unit_id);
			}
		}
		else
		{
			$return = false;
		}

		if($return)
		{
			$this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
		}else{
			$this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
		}


		redirect($this->session->userdata('redirect'));
	}

	function delete_unit_surveillance_not_exist($okupasi_unit_surveillance_id =null, $arr_unit_id = array())
	{
		if(!is_null($okupasi_unit_surveillance_id))
		{
			$query_unit = $this->unit_kompetensi_surveillance_db->get_all(array('okupasi_unit_surveillance_id' => $okupasi_unit_surveillance_id));
			if($query_unit)
			{
				foreach($query_unit as $unit)
				{
					$id = $unit['_id']->{'$id'};
					if(!in_array($id, $arr_unit_id))
					{
						$this->unit_kompetensi_surveillance_db->delete($id);
					}
				}
			}
		}

	}

    function setting_filter_join_exam($content = 'home'){
        check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
        $data['title_page'] = 'Setting Pendaftaran Ujian';
        $data['title'] = $this->config->item('title').' : '.$data['title_page'];

        if($content == 'home'){
            $this->session->set_userdata('redirect',current_url());
            $data['max_day_join_exam'] = isset($this->setting_db->get('max_bussinesday_join_exam')[0]['day']) ?
                $this->setting_db->get('max_bussinesday_join_exam')[0]['day'] : 0;

            $data['max_day_join_next_exam'] = isset($this->setting_db->get('max_bussinesday_join_next_exam')[0]['day']) ?
                $this->setting_db->get('max_bussinesday_join_next_exam')[0]['day'] : 0;

            $data['min_amount_participant_exam'] = isset($this->setting_db->get('min_amount_participant_exam')[0]['limit']) ?
                $this->setting_db->get('min_amount_participant_exam')[0]['limit'] : 0;
				
			$data['min_days_reschedule'] = isset($this->setting_db->get('min_days_reschedule')[0]['day']) ?
                $this->setting_db->get('min_days_reschedule')[0]['day'] : 0;

			$data['url_inpassing'] = isset($this->setting_db->get('url_inpassing')[0]['apl_inpassing']) ?
                $this->setting_db->get('url_inpassing')[0]['apl_inpassing'] : 0;
				
            $data['title_content'] = 'Edit';
            $data['content'] = 'setting_join_exam_add';
        }

        $this->load->view('admin/main',$data);
    }

    function setting_filter_join_exam_post(){
        $input = $this->input->post();
        $status = false;
        foreach ($input as $key => $item) {
            $data['setting_id'] = $key;
            $setting_id = $data['setting_id'];
            if($key == 'min_amount_participant_exam'){
                $data['limit'] = new MongoInt32($item);
            }else{

                $data['day'] = new MongoInt32($item);
            }

            if($this->setting_db->save($setting_id, $data)){
                $status = true;
            }else{
                $status = false;
                break;
            }
            unset($data);
        }

        if($status){
            $this->session->set_flashdata('notif', array('status' => true, 'msg' => 'Sukses!'));
        }else{
            $this->session->set_flashdata('notif', array('status' => false, 'msg' => 'Gagal!'));
        }

        redirect($this->session->userdata('redirect'));
    }
	
	function setting_upload_sqlold_view(){
        check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
        $data['title_page'] = 'Upload Sql Sistem Lama';
        $data['title'] = $this->config->item('title').' : '.$data['title_page'];

        $data['title_content'] = 'Upload';
        $data['content'] = 'setting_upload_sql_lama';
        
        $this->load->view('admin/main',$data);
    }

	function setting_upload_sql_view(){
        check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
        $data['title_page'] = 'Upload Data From SQL Script';
        $data['title'] = $this->config->item('title').' : '.$data['title_page'];

        $data['title_content'] = 'Upload';
        $data['content'] = 'setting_upload_sql';
        
        $this->load->view('admin/main',$data);
    }
	
	function setting_upload_sql()
	{
		$file_loc_sql       = file_exists($_FILES['file_sql']['tmp_name']) ? $_FILES['file_sql']['tmp_name'] : null;
        $fp_sql             = !is_null($file_loc_sql) ? fopen($file_loc_sql, 'r') : null;
        $content_sql        = !is_null($fp_sql) ? fread($fp_sql, filesize($file_loc_sql)) : null;
        $data_sql           = !is_null($content_sql) ? explode(";", $content_sql) : null;
		

		$pattern  =  '/insert into (\S+) \((.*)\) values \((.*)\);$/';
        $status   = false;

		if(!is_null($data_sql))
		{
			for ($i=0; $i < count($data_sql)-1; $i++)
			{

                $str = $data_sql[$i].";";
				preg_match($pattern,$str , $out);
                $result = $this->mapping_value($out);
								
				$data_content = array();
				
				$data_content['prp_id'] = isset($result['prp_id']) ?  $result['prp_id'] : "";
				$data_content['prp_nama'] = isset($result['prp_nama']) ?  $result['prp_nama'] : "";
				$data_content['jmlpemegangsertifikat'] = isset($result['jmlpemegangsertifikat']) ?  $result['jmlpemegangsertifikat'] : "";
				
				$data_content['deleted'] = 0;
				$this->training_db->save_master_prp("",$data_content);
            }
        }
	}
	
	function setting_upload_sql_lama()
	{

	$file_loc_sql_lama       = file_exists($_FILES['file_sql_lama']['tmp_name']) ? $_FILES['file_sql_lama']['tmp_name'] : null;
        $fp_sql_lama             = !is_null($file_loc_sql_lama) ? fopen($file_loc_sql_lama, 'r') : null;
        $content_sql_lama        = !is_null($fp_sql_lama) ? fread($fp_sql_lama, filesize($file_loc_sql_lama)) : null;
        $data_sql_lama           = !is_null($content_sql_lama) ? explode(";", $content_sql_lama) : null;
		

		$pattern  =  '/insert into (\S+) \((.*)\) values \((.*)\);$/';
        $status   = false;

		if(!is_null($data_sql_lama))
		{
			for ($i=0; $i < count($data_sql_lama)-1; $i++)
			{

                $str = $data_sql_lama[$i].";";

                preg_match($pattern,$str , $out);
                $result = $this->mapping_value($out);

				$training_id = $result['id_mongo'];
								
				$data_content = array();
				
				$data_content['training_id'] = isset($training_id) ?  $training_id : ""; 
				$data_content['training_log'] = $this->training_db->get_all($training_id)[0];
				$data_content['institute_approved'] = 1;
				$data_content['instructor_approved_log'] = array();
				
				$data_content['author']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['author']['entity'] = "peserta";
				$data_content['author']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";
				$data_content['author']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['author']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['author']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['author']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['author']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['author']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['author']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				
				$data_content['creator']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['creator']['entity'] = "peserta";
				$data_content['creator']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";;
				$data_content['creator']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['creator']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['creator']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['creator']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['creator']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['creator']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['creator']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				
				$data_content['user_join']['exam_number'] = isset($result['no_ujian_ppsdm']) ?  $result['no_ujian_ppsdm'] : "";
				$data_content['user_join']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['user_join']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['user_join']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				$data_content['user_join']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['user_join']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['user_join']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['user_join']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['user_join']['last_education'] = isset($result['pendidikan_terakhir']) ?  $result['pendidikan_terakhir'] : "";
				$data_content['user_join']['academic_degree'] = isset($result['gelar']) ?  $result['gelar'] : "";
				$data_content['user_join']['employment'] = isset($result['status_pegawai']) ?  $result['status_pegawai'] : "";
				$data_content['user_join']['nip_id'] = isset($result['nip']) ?  $result['nip'] : "";
				$data_content['user_join']['ktp_id'] = isset($result['no_ktp']) ?  $result['no_ktp'] : "";
				$data_content['user_join']['institution'] = isset($result['satker']) ?  $result['satker'] : "";		
				$data_content['user_join']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['user_join']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";

				$data_content['deleted'] = 0;
				var_dump($data_content);
				$this->join_exam_db->save("",$data_content);
            }
        }
	}
	
//---------------------------------------------------------------------------------------------------------------------

	function setting_upload_sql_inpassing()
	{
		
		
		$file_loc_sql_inpassing  = file_exists($_FILES['file_sql_inpassing']['tmp_name']) ? $_FILES['file_sql_inpassing']['tmp_name'] : null;
        $fp_sql_inpassing        = !is_null($file_loc_sql_inpassing) ? fopen($file_loc_sql_inpassing, 'r') : null;
        $content_sql_inpassing   = !is_null($fp_sql_inpassing) ? fread($fp_sql_inpassing, filesize($file_loc_sql_inpassing)) : null;
        $data_sql_inpassing      = !is_null($content_sql_inpassing) ? explode(";", $content_sql_inpassing) : null;
		$data_inpassing = array();
		
		
		$file_loc_sql_inpassing_jadwal  = file_exists($_FILES['file_sql_jadwal_inpassing']['tmp_name']) ? $_FILES['file_sql_jadwal_inpassing']['tmp_name'] : null;
        $fp_sql_inpassing_jadwal        = !is_null($file_loc_sql_inpassing_jadwal) ? fopen($file_loc_sql_inpassing_jadwal, 'r') : null;
        $content_sql_inpassing_jadwal   = !is_null($fp_sql_inpassing_jadwal) ? fread($fp_sql_inpassing_jadwal, filesize($file_loc_sql_inpassing_jadwal)) : null;
        $data_sql_inpassing_jadwal      = !is_null($content_sql_inpassing_jadwal) ? explode(";", $content_sql_inpassing_jadwal) : null;
		$data_inpassing_jadwal = array();
		
		
		$pattern  =  '/insert into (\S+) \((.*)\) values \((.*)\);$/';
        $status   = false;

		if(!is_null($data_sql_inpassing))
		{
			for ($i=0; $i < count($data_sql_inpassing)-1; $i++)
			{

                $str = $data_sql_inpassing[$i].";";

                preg_match($pattern,$str , $out);
                $result = $this->mapping_value($out);
				$data_inpassing = $result;
				
				

				/*
				$training_id = $result['id_mongo'];
								
				$data_content = array();
				
				$data_content['training_id'] = isset($training_id) ?  $training_id : ""; 
				$data_content['training_log'] = $this->training_db->get_all($training_id)[0];
				$data_content['institute_approved'] = 1;
				$data_content['instructor_approved_log'] = array();
				
				$data_content['author']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['author']['entity'] = "peserta";
				$data_content['author']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";
				$data_content['author']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['author']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['author']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['author']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['author']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['author']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['author']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				
				$data_content['creator']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['creator']['entity'] = "peserta";
				$data_content['creator']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";;
				$data_content['creator']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['creator']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['creator']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['creator']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['creator']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['creator']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['creator']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				
				$data_content['user_join']['exam_number'] = isset($result['no_ujian_ppsdm']) ?  $result['no_ujian_ppsdm'] : "";
				$data_content['user_join']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['user_join']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['user_join']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				$data_content['user_join']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['user_join']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['user_join']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['user_join']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['user_join']['last_education'] = isset($result['pendidikan_terakhir']) ?  $result['pendidikan_terakhir'] : "";
				$data_content['user_join']['academic_degree'] = isset($result['gelar']) ?  $result['gelar'] : "";
				$data_content['user_join']['employment'] = isset($result['status_pegawai']) ?  $result['status_pegawai'] : "";
				$data_content['user_join']['nip_id'] = isset($result['nip']) ?  $result['nip'] : "";
				$data_content['user_join']['ktp_id'] = isset($result['no_ktp']) ?  $result['no_ktp'] : "";
				$data_content['user_join']['institution'] = isset($result['satker']) ?  $result['satker'] : "";		
				$data_content['user_join']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['user_join']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";;

				*/
				$data_inpassing['deleted'] = 0;
				
				$this->todolist_db->save_peserta("",$data_inpassing);
            }
        }
		
		
		
		if(!is_null($data_sql_inpassing_jadwal))
		{
			for ($i=0; $i < count($data_sql_inpassing_jadwal)-1; $i++)
			{
                $str = $data_sql_inpassing_jadwal[$i].";";

                preg_match($pattern,$str , $out);
                $result = $this->mapping_value($out);
				$data_inpassing_jadwal = $result;
				
				

				/*
				$training_id = $result['id_mongo'];
								
				$data_content = array();
				
				$data_content['training_id'] = isset($training_id) ?  $training_id : ""; 
				$data_content['training_log'] = $this->training_db->get_all($training_id)[0];
				$data_content['institute_approved'] = 1;
				$data_content['instructor_approved_log'] = array();
				
				$data_content['author']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['author']['entity'] = "peserta";
				$data_content['author']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";
				$data_content['author']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['author']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['author']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['author']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['author']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['author']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['author']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				
				$data_content['creator']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['creator']['entity'] = "peserta";
				$data_content['creator']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";;
				$data_content['creator']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['creator']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['creator']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['creator']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['creator']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['creator']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['creator']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				
				$data_content['user_join']['exam_number'] = isset($result['no_ujian_ppsdm']) ?  $result['no_ujian_ppsdm'] : "";
				$data_content['user_join']['username'] = isset($result['username']) ?  $result['username'] : "";
				$data_content['user_join']['email'] = isset($result['email']) ?  $result['email'] : "";
				$data_content['user_join']['fullname'] = isset($result['nama']) ?  $result['nama'] : "";
				$data_content['user_join']['birth_place'] = isset($result['tempat_lahir']) ?  $result['tempat_lahir'] : "";
				$data_content['user_join']['birth_date'] = isset($result['tgl_lahir']) ?  $result['tgl_lahir'] : "";
				$data_content['user_join']['phone'] = isset($result['telepon_hp']) ?  $result['telepon_hp'] : "";
				$data_content['user_join']['gender'] = isset($result['jenis_kelamin']) ?  $result['jenis_kelamin'] : "";
				$data_content['user_join']['last_education'] = isset($result['pendidikan_terakhir']) ?  $result['pendidikan_terakhir'] : "";
				$data_content['user_join']['academic_degree'] = isset($result['gelar']) ?  $result['gelar'] : "";
				$data_content['user_join']['employment'] = isset($result['status_pegawai']) ?  $result['status_pegawai'] : "";
				$data_content['user_join']['nip_id'] = isset($result['nip']) ?  $result['nip'] : "";
				$data_content['user_join']['ktp_id'] = isset($result['no_ktp']) ?  $result['no_ktp'] : "";
				$data_content['user_join']['institution'] = isset($result['satker']) ?  $result['satker'] : "";		
				$data_content['user_join']['user_id'] = isset($result['user_id']) ?  $result['user_id'] : "";
				$data_content['user_join']['photo'] = isset($result['ppsdm_photo']) ?  $result['ppsdm_photo'] : "";;

				*/
				$data_inpassing_jadwal['deleted'] = 0;
				
				$this->todolist_db->save_jadwalinp("",$data_inpassing_jadwal);
            }
        }
	}

//---------------------------------------------------------------------------------------------------------------------	
	
	function mapping_value($data){
        $model = [];
        $field = explode(",", $data[2]);
        $value = explode(",",str_replace("'","", $data[3]));

        if(count($field) == count($value)){
            for ($i=0; $i < count($field) ; $i++) {
                $model[$field[$i]] = $value[$i];
            }
        }

        return $model;
    }
	
	function setting_district($content = 'home')
	{
		check_token($this->connect_auth->get_access_token(), $allow_role = array('superadmin'));
		$data['title_page'] = 'Setting District/Kota';
		$data['title'] = $this->config->item('title').' : '.$data['title_page'];
		
		if($content == 'home'){
			$this->session->set_userdata('redirect',current_url());
			$data['title_content'] = 'Input';
			$data['content'] = 'setting_district';
		}

		$this->load->view('admin/main',$data);
    }
	
	function district_setting_post(){
		$input = $this->input->post();
		$id = $input['id_jadwal'];
		$district_id = $input['training_location_district'];
		$data_content = $this->training_db->get($id);
		
		if(isset($district_id))
		{
			$arr_district = explode("-",$district_id);
		}
		
		if(isset($data_content)){
			$content = $data_content[0];
			$content['training_location_district']['id'] = isset($content['training_location_district']['id']) ? $content['training_location_district']['id'] : $arr_district[0];
			$content['training_location_district']['name'] = isset($content['training_location_district']['name']) ? $content['training_location_district']['name'] : $arr_district[1];
			$content['exam_location_district']['id'] = isset($content['exam_location_district']['id']) ? $content['exam_location_district']['id'] : $arr_district[0];
			$content['exam_location_district']['name'] = isset($content['exam_location_district']['name']) ? $content['exam_location_district']['name'] : $arr_district[1];
			$save_content = $this->training_db->save($id, $content);
		}
		
		redirect($this->session->userdata('redirect'));
    }
	
	function get_data_tabelnilai()
	{
		$datapeserta = $this->setting_db->get_sqldata_tabelnilai(array("flag" => 1));

		foreach($datapeserta as $row)
		{
		
			$updating_data = $this->setting_db->update_by_id($row);
		}	
	}
	
	public function export_data_lulus($exam_id = null)
	{
        check_token($this->connect_auth->get_access_token(), $allow_role = array('admin_pencetakan'));

        $query = $this->join_exam_db->get_all(array('training_id' => $exam_id,'evaluation_exam' => 1,'institute_approved' => 1));
        if($query){
            foreach($query as $key => $row){
                $no = $key+1;
                $no_ujian = isset($row['user_join']['exam_number']) ? $row['user_join']['exam_number'] : "";
                $nama = isset($row['user_join']['fullname']) ? $row['user_join']['fullname'] : "";
                $gelar = isset($row['user_join']['academic_degree']) ? $row['user_join']['academic_degree'] : "";
                $nip = isset($row['user_join']['nip_id']) ? $row['user_join']['nip_id'] : "";
                $telepon_hp = isset($row['user_join']['phone']) ? $row['user_join']['phone'] : "";

                $data_ujian[] = array(
                    'no' => $no,
                    //'no_ujian' => '"'.$no_ujian.'"',
					'no_ujian' => '".'.$no_ujian.'"',
                    'nama' => '"'.$nama.'"',
                    'gelar' => '"'.$gelar.'"',
                    'nip' => '".'.$nip.'"',
                    'telepon_hp' => '".'.$telepon_hp.'"',
                );
            }

            $filename = "peserta_ujian_lulus".date('d/m/Y',time()).".csv";
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=\"$filename\"");
            header("Expires: 0");
            ob_clean();
            $this->ExportCSVFile($data_ujian);

            exit();
        }
    }
	
	function ExportCSVFile($records) {
        // create a file pointer connected to the output stream
        $fh = fopen( 'php://output', 'w' );
        $heading = false;
        if(!empty($records))
            foreach($records as $row) {
                if(!$heading) {
                    // output the column headings
                    echo implode(";", array_keys($row)) . "\r\n";
                    //fputcsv($fh, array_keys($row));
                    $heading = true;
                }
                // loop over the rows, outputting them
                echo implode(";", array_values($row)) . "\r\n";
                //fputcsv($fh, array_values($row));

            }
        fclose($fh);
    }
	
	function setting_exam_date()
	{
		$new_exam_date = isset($_GET['new_exam_date']) ? $_GET['new_exam_date'] : "";
		$join_exam_id = isset($_GET['join_exam_id']) ? $_GET['join_exam_id'] : "";
		$getdatapeserta = $this->join_exam_db->get_all(array("join_exam_id" => $join_exam_id));
		$data_content = $getdatapeserta[0];
		$data_content['training_log']['exam_date'] = $new_exam_date;
		$result = $this->join_exam_db->save($join_exam_id,$data_content);
		return $result;
	}

	function get_api_user()
	{
		$join_exam_id = isset($_GET['join_exam_id']) ? $_GET['join_exam_id'] : "";
		$getdata = $this->join_exam_db->get_all(array("join_exam_id" => $join_exam_id));
		$data_content = $getdata[0];
		$username = $data_content['user_join']['username'];
		$url=$this->config->item('connect_url').'/user/get_data_user?username='.$username;
		$response = call_api_get($url);
		$body=json_decode($response['body'],true);
		$connect_data_user = $body[0];
		$data_content['user_join']['photo'] = $connect_data_user['photo'];
		$data_content['user_join']['nip_id'] = $connect_data_user['nip_id'];
		$data_content['user_join']['ktp_id'] = $connect_data_user['ktp_id'];
		$data_content['user_join']['academic_degree'] = $connect_data_user['academic_degree'];
		$result = $this->join_exam_db->save($join_exam_id,$data_content);
		return true;
	}

	public function get_data()
	{
		$draw=$_REQUEST['draw'];
		$length = (int) $_REQUEST['length'];
		$start = (int) $_REQUEST['start'];
		$search = $_REQUEST['search']["value"];
		//$total=$this->db->count_all_results("lb_pemegangsertifikat");
		$output['data'] = array();
		$arrdata = array();
		$state = $_REQUEST['state'];
		$arrdata = $this->logbook_db->get_detailprovince_procurement_expert($state, $start, $length, $search);
		
		$output['recordsTotal']=$output['recordsFiltered']=$this->logbook_db->count_detailprovince_procurement_expert($state);
		$nomor_urut = 1;
		foreach ($arrdata as $content)
		{
			$id = isset($content['id']) ? $content['id'] : "";
			$url_participant = "statistics/procurement_detail_result_namadetail?state_id=$state&id=$id";
			$url = site_url($url_participant);
			$button_detail = "<div class=margin-bottom-5>
				<a href='$url' class='btn default btn-xs'><i class=fa fa-eye></i> Lihat Detail</a>
				</div>";
			$tgl_lahir = tgl_indo($content['tgl_lahir']);
			$output['data'][]=array($nomor_urut, $content['nama'], $content['nip'], $tgl_lahir, $content['tempat_lahir'], $button_detail);
			$nomor_urut++;
		}
		
		echo json_encode($output);
	}
	
	public function get_expert_result()
	{
		$draw=$_REQUEST['draw'];
		$length = (int) $_REQUEST['length'];
		$start = (int) $_REQUEST['start'];
		$search = $_REQUEST['search']["value"];
		$output['data'] = array();
		$arrdata = array();
		$state = $_REQUEST['searchfield'];
		$arrdata = $this->logbook_db->get_detail_expert_result($state, $start, $length, $search);
		$output['recordsTotal']=$output['recordsFiltered']=$this->logbook_db->count_detail_expert_result($state);
		$nomor_urut = 1;
		foreach ($arrdata as $content)
		{
			$id = isset($content['id']) ? $content['id'] : "";
			$url_participant = "statistics/procurement_detail_result_namadetail?id=$id";
			$url = site_url($url_participant);
			$button_detail = "<div class=margin-bottom-5>
				<a href='$url' class='btn default btn-xs'><i class=fa fa-eye></i> Lihat Detail</a>
				</div>";
				
			$tgl_lahir = tgl_indo($content['tgl_lahir']);
			$output['data'][]=array($nomor_urut, $content['nama'], $content['nip'], $tgl_lahir, $content['tempat_lahir'], $button_detail);
			$nomor_urut++;
		}
		
		echo json_encode($output);
	}
	
	public function pemegang_sertifikat()
	{
		
		$training_id = isset($_GET['training_id']) ? $_GET['training_id'] : "";		
		$data_join_exam = $this->join_exam_db->get_all(array('training_id' => $training_id,'evaluation_exam' => 1,'institute_approved' => 1,'published' => 1,'evaluation_exam' => 1));
		foreach($data_join_exam as $data)
		{
			$filter['id'] = isset($data['id']) ? $data['id'] : $data['training_id'];
			$data_content = $this->join_exam_db->get_all($filter);
			$counter = (int) $this->sertifikat_peserta_ujian_dasar_db->count_pemilik_data_sertifikat($data['certificate_number'])[0]['pemiliksertifikat'];
			$degree = isset($data['user_join']['academic_degree']) ? $data['user_join']['academic_degree'] : "";
			
			
			if(is_array($degree))
			{
				$data['user_join']['academic_degree'] = "";
				foreach($degree as $a)
				{
					$data['user_join']['academic_degree'] .= $a; 
				}
			}

			if($counter > 0)
			{
				$this->sertifikat_peserta_ujian_dasar_db->update_pemilik_data_sertifikat($data,$data['certificate_number']);
			}
			else
			{
				$this->sertifikat_peserta_ujian_dasar_db->insert_pemilik_data_sertifikat($data,$data['certificate_number']);
			}
			
			/*
			$filter['id'] = "5844e82e7c1e9ab921b45a1c";
			$filter['id'] = $data['id'];
			$data_content = $this->join_exam_db->get_all($filter);
			$this->sertifikat_peserta_ujian_dasar_db->update_pemilik_data_sertifikat($data_content[0],$data_content[0]['certificate_number']);
			*/
		}
	}


}

/* End of file pages.php */
/* Location: ./application/controllers/superadmin/pages.php */
